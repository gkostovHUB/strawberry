﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CarsController : MonoBehaviour
{
    [SerializeField] private CarsDatabase m_CarsDatabase;
    [SerializeField] private List<Sprite> m_Clients;
    [SerializeField] private Image m_CurrentClientImage;
    [SerializeField] private AudioSource m_Audio;
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (transform.GetChild(0).childCount > 0)
            {
                Destroy(transform.GetChild(0).GetChild(0).gameObject);
                GameObject.Find("CarProblems").GetComponent<Text>().text = "";
            }
            Instantiate(m_CarsDatabase.Cars[Random.Range(0, m_CarsDatabase.Cars.Count)], transform.position, Quaternion.identity, transform.GetChild(0));

            m_CurrentClientImage.sprite = m_Clients[Random.Range(0, m_Clients.Count)];
            m_Audio.Play();
        }       
    }
}