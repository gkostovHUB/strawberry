﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] private GameObject MainMenu;
    [SerializeField] private GameObject Credits;
    [SerializeField] private GameObject EscapeMenu;
    [SerializeField] private GameObject InGameMenu;
    public void NewGame()
    {
        SceneManager.LoadScene("Garage");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void SetCredits(bool show)
    {
        MainMenu.SetActive(!show);
        Credits.SetActive(show);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            EscapeMenu.SetActive(!EscapeMenu.activeSelf);
            InGameMenu.SetActive(!EscapeMenu.activeSelf);
        }
    }

    public void GotToMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}