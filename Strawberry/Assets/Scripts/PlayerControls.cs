﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    [Range(2.0f, 10.0f)][SerializeField] private float m_PlayerSpeed = 5.0f;
    private Vector3 m_TargetPosition;

    private Camera m_Camera;
    RaycastHit m_TartgetRaycast;

    private Animator m_PlayerAnimator;

    void Start()
    {
        m_Camera = Camera.main;
        m_TargetPosition = transform.position;
        m_TartgetRaycast.point = transform.position;
        m_PlayerAnimator = GetComponent<Animator>();
    }
    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out m_TartgetRaycast, 100))
            {
                Vector3 temp = m_TartgetRaycast.point;
                temp.y = 0.13f;
                m_TartgetRaycast.point = temp;
                m_TargetPosition = m_TartgetRaycast.point;
            }
        }
        transform.position = Vector3.MoveTowards(transform.position, m_TargetPosition, Time.deltaTime * m_PlayerSpeed);
        transform.LookAt(m_TartgetRaycast.point);
        m_PlayerAnimator.SetBool("IsWalking", (transform.position != m_TargetPosition));
    }
}