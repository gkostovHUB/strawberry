﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform m_Player;
    private bool m_WatchPlayer = false;
    [SerializeField] private GameObject UI;

    void LateUpdate()
    {
        if (m_WatchPlayer)
        {
            transform.LookAt(m_Player.transform);
        }
    }

    public void WatchPlayer()
    {
        m_WatchPlayer = true;
        UI.SetActive(true);
    }
}