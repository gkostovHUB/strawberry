﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCarsDatabase", menuName = "Cars/NewCar")]
public class CarsDatabase : ScriptableObject
{
    [SerializeField] private List<GameObject> m_Cars;
    public List<GameObject> Cars { get { return m_Cars; } }
}
