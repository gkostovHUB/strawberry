﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class MoneyGoal : MonoBehaviour
{
    public Image foregroundImage;
    public Clock clock;
    public int dailyGoal = 0;
    public int totalMoney = 0;
    public Text uiMoneyAmount;
    public Text uiDailyMoney;

    public int Value
    {
        get
        {
            if (foregroundImage != null)
                return (int)(foregroundImage.fillAmount * 100);
            else
                return 0;
        }
        set
        {
            if (foregroundImage != null)
                foregroundImage.fillAmount = value / 100f;

            if(_Value < dailyGoal && value >= dailyGoal)
            {
                //End day logic 
            }

            _Value = value;
        }
    }

    private int _Value = 0;

    void Start()
    {
        //dailyGoal = 0;
        //StartCoroutine(MyRoutine());
    }

    IEnumerator MyRoutine()
    {
        clock.enabled = true;
        while(clock.hour < 17)
        {
            yield return null;
        }
        clock.enabled = false;
        dailyGoal = 0;
    }

    //adds amount to the daily and total money goal. Updates UI.
    public void getMoney(int amount)
    {
        dailyGoal += amount;
        totalMoney += amount;
        uiMoneyAmount.text = totalMoney.ToString() + " $";
        uiDailyMoney.text = dailyGoal.ToString() + " $";
        Value += dailyGoal * 100 / 10000;
    }
}
