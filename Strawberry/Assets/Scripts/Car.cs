﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Car : MonoBehaviour
{
    [SerializeField] private Transform m_TargetPosition;
    [Range(2.0f, 12.0f)][SerializeField] private float m_CarSpeed;

    Text uiCarProblems;
    public List<string> problems = new List<string>
    {
        "Faulty Battery",
        "Bad wiring",
        "Oil change",
        "Broken starter",
        "New sparkles",
        "Flat tire",
        "Faulty gears",
        "Air filter change",
        "Brake fluid",
        "Faulty brakes",
        "Clutch change"
    };

    public List<string> problemsToFix;
    //private List<string> ShuffleList(List<string> inputList)
    //{
    //    List<string> randomList = new List<string>();

    //    System.Random r = new System.Random();
    //    int randomIndex = 0;
    //    while (inputList.Count > 0)
    //    {
    //        randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
    //        randomList.Add(inputList[randomIndex]); //add it to the new, random list
    //        inputList.RemoveAt(randomIndex); //remove to avoid duplicates
    //    }

    //    return randomList; //return the new random list
    //}

    List<string> ReturnRandProblems(int count, List<string> lt)
    {

        List<string> RandomItems = new List<string>();

        System.Random random = new System.Random();

        HashSet<string> hs = new HashSet<string>();
        do
        {
            hs.Add(lt[random.Next(0, lt.Count)]);
        }
        while(hs.Count < count);

        List<string> result = hs.ToList();

        return result;
    }
    //change to select 3-4 at random.
    public void show()
    {
        List<string> currentProblems = ReturnRandProblems(3, problems);
        problemsToFix = currentProblems;
        foreach (string item in currentProblems)
        {
            uiCarProblems.text += item.ToString() +" \n";
        }
    }

    private void Start()
    {
        uiCarProblems = GameObject.Find("CarProblems").GetComponent<Text>();
        m_TargetPosition = GameObject.Find("Cars").transform.GetChild(0).transform;

        GameObject.Find("InventoryController").GetComponent<InventoryController>().myCar = this;
    }
    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, m_TargetPosition.position, Time.deltaTime * m_CarSpeed);
        if(transform.position == m_TargetPosition.position)
        {
            //Update UI with reference.
            if (uiCarProblems.text.Length == 0)
            {
                show();
                //TODO - Reset the text in uiCarProblems.text after the repairs are executed.
            }
        }
    }
}