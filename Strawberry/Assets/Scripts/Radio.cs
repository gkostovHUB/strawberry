﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : MonoBehaviour
{
    [SerializeField] private List<AudioClip> m_RadioMusic;
    private static int i = 0;
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            i = (i >= m_RadioMusic.Count-1) ? 0 : ++i;
            GetComponent<AudioSource>().clip = m_RadioMusic[i];
            GetComponent<AudioSource>().Play();
        }
    }
}