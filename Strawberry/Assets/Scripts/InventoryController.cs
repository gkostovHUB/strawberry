﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    Dictionary<string,int> inventory = new Dictionary<string, int>();
    // TODO - refetence to CAR
    public Car myCar;
    //TODO - reference to MoneyGoals
    public MoneyGoal myMoney;

    // Start is called before the first frame update
    void InitInventory(Dictionary<string, int> m_inventory)
    {
        m_inventory.Add("Battery", 5);
        m_inventory.Add("Wiring", 5);
        m_inventory.Add("Oil", 5);
        m_inventory.Add("Starter", 5);
        m_inventory.Add("SparkPlugs", 10);
        m_inventory.Add("Tires", 6);
        m_inventory.Add("GearBox", 4);
        m_inventory.Add("AirFilter", 5);
        m_inventory.Add("BrakeFluid", 3);
        m_inventory.Add("Ferodo", 6);
        m_inventory.Add("Clutch", 5);
    }

    void RepairCar(List<string> nprProblems, Dictionary<string, int> m_inventory)
    {
        foreach (string problem in nprProblems)
        {
            switch (problem.ToString())
            {
                case "Faulty Battery": removeItem("Battery", 1, m_inventory);break;
                case "Bad wiring": removeItem("Wiring", 1, m_inventory); break;
                case "Oil change": removeItem("Oil", 1, m_inventory); break;
                case "Broken starter": removeItem("Starter", 1, m_inventory); break;
                case "New sparkles": removeItem("SparkPlugs", 1, m_inventory); break;
                case "Flat tire": removeItem("Tires", 1, m_inventory); break;
                case "Faulty gears": removeItem("GearBox", 1, m_inventory); break;
                case "Air filter change": removeItem("AirFilter", 1, m_inventory); break;
                case "Brake fluid": removeItem("BrakeFluid", 1, m_inventory); break;
                case "Faulty brakes": removeItem("Ferodo", 1, m_inventory); break;
                case "Clutch change": removeItem("Clutch", 1, m_inventory); break;
                default: Debug.Log("Nothing in stock!!!"); break;
            }
        }
    }

    void removeItem(string itemName, int quantity,  Dictionary<string, int> m_inventory)
    {
        if(m_inventory[itemName] >= quantity)
        {
            m_inventory[itemName] -= quantity;
            //TODO Daily money ++ 
            myMoney.getMoney(200);
        }
        else
        {
            //show in a UI element. reduce price
            Debug.Log("Not enough " + itemName);
        }
    }

    public void DoRepairs()
    {
        RepairCar(myCar.problemsToFix, inventory);
    }

    void Start()
    {
        InitInventory(inventory);
        //myMoney.getMoney(200);
    }

    // Update is called once per frame
    void Update()
    {
      
    }
    
}
